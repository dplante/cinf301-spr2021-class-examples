import React from 'react';

/*
* A simple class that just renders the React component from this.props
*/
export default function Display(props) {
    return (
        <div>
            <h1>Hello {props.stats.name}</h1>
            <h1>You are {props.stats.age}</h1>
            <h1>Your weight is {props.stats.weight}</h1>
        </div>
    );
}
