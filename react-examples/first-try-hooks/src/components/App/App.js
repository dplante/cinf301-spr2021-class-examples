import React, { useState } from 'react';
import './App.css';
import Display from '../Display/Display.js';

export default function App() {
  const [name, setName] = useState('Julie');
  const [age, setAge] = useState('aging well');
  const [weight, setWeight] = useState(180);

  function clicker(name) {
    setName(name);
    if (name.localeCompare("Daniel") === 0) {
      setAge('TOO OLD');
    } else {
      setAge('not as old as Daniel');
    }
  }

  function renderDisplay(data) {
    return <Display stats={data} />;
  }

  function renderButton() {
    return (
      <button onClick={() => clicker(document.getElementById('fname').value)}>
        {name}
      </button>
    );
  }

  const data = { name: name, age: age, weight: weight };
  return (
    <div>
      <div>Enter a name: <input type="text" id="fname" name="fname" /></div>
      <div>{renderDisplay(data)}</div>
      <div>{renderButton("Button")}</div>
    </div>
  );

}
