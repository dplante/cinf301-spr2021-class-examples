/**
 * Demo Javascript code for rolling a single die or collection of dice.
 *
 */

let gameNum = 1;

document.getElementById('buttn').onclick = function () {
    let maxValue = 1;

    for (let i = 0; i < dice.numberOfDice; i++) {
        dice.dice[i].roll();
        if (dice.dice[i].top > maxValue) {
            maxValue = dice.dice[i].top;
        }
        document.getElementById('d' + i).src = dice.dice[i].img.src;
    }
    updateTable(maxValue);

};

/**
 * When die is selected, add or remove border; note that this is handled
 * differently with different browsers
 */
function dieClicked(event) {
    let tid = event.target.id;
    let element = document.getElementById(tid);
    element.isSelected = !element.isSelected;
    let styles = window.getComputedStyle(element);
    let borderProp = styles.getPropertyValue('border');

    if (element.isSelected) {
        element.style.border = '2px solid #021a40';
    } else {
        element.style.border = 'none';
    }

}
// function dieClicked(event) {
//     let tid = event.target.id;
//     let element = document.getElementById(tid);
//     let styles = window.getComputedStyle(element);
//     let borderProp = styles.getPropertyValue('border');
//
//     if (borderProp === '0px none rgb(0, 0, 0)') {
//         element.style.border = '2px solid #021a40';
//     } else {
//         element.style.border = 'none';
//     }
//
// }
//
/**
 * Dynamically update the table, adding a row indicating the max value
 * of die rolled for each 'game'
 *
 * @param val
 */
let updateTable = function (val) {
    let scores = document.getElementById('tableScores');
    let row = scores.insertRow(gameNum);
    gameNum++;
    row.insertCell(0).innerHTML = 'Game #' + gameNum;
    row.insertCell(1).innerHTML = 'Max Roll: ' + val.toString();
};

/*
 * Prototypes
 */
function Die(top) {
    this.top = top;
    this.img = new Image();
    this.img.src = 'images/d' + top + '.png';
    this.isSelected = false;
}

Die.prototype.roll = function () {
    this.top = Math.floor(Math.random() * 6) + 1;
    this.img.src = 'images/d' + this.top + '.png';
};


function Dice(numDice) {
    this.numberOfDice = numDice;
    this.dice = [];
    this.create = function () {
        for (let i = 0; i < this.numberOfDice; i++) {
            this.dice.push(new Die(i));
            document.getElementById('d' + i).addEventListener('click', dieClicked);
        }
    };
    this.create();
}

let dice = new Dice(5);
